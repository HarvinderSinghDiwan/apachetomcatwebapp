<html>
<body>
<h2>Hello World: src/main/webapp/index.jsp</h2>
<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="">
<!-- Start LMT Header Section V3 -->

<head>
    <script id="dpal" src="//www.redhat.com/dtm.js" type="text/javascript"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <title>Red Hat Training - HARVINDERSINGHDIWAN (Harvinder Singh Diwan)</title>
    <meta name="lang" content="en" />
    <meta name="region" content="in" />
    <meta name="siteid" content="redhat" />
    <meta name="portid" content="1" />
    <meta name="sessid" content="J5025J4018020J20" />
    <meta name="toolkitversion"content="6.17.47" />
    <meta name="uimodule" content="current-learning" />
    <meta name="cartplugin"content="Y" />

    <!-- start COMMON LIBRARY V2 -->
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, minimum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="icon" type="image/png" href="/st_toolkit/redhat/favicon.png?6.17.47" />
   <script type="text/javascript" src="/st_toolkit/common/script/_thirdparty/lib/jquery-polyfilled.js?6.17.47"></script>

    <link rel="stylesheet" type="text/css" href="/st_toolkit/common/css/normalise.css?6.17.47" />
    <link rel="stylesheet" type="text/css" href="/st_toolkit/redhat/css/page__header.css?6.17.47" /> 
    <link rel="stylesheet" type="text/css" href="/st_toolkit/common/css/page__footer.css?6.17.47" />
    <link rel="stylesheet" type="text/css" href="/st_toolkit/common/css/pages/current-learning.css?6.17.47" />
    <link rel="stylesheet" type="text/css" href="./style.css" />    
    
    
    
 
    <script type="text/javascript">
        var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
        if (isIE11) {

            //injecting polyfills js
            var head = document.getElementsByTagName('head')[0];
            var firstScriptTag = head.querySelector('script');
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = '/st_toolkit/common/script/2019/polyfills/ie11-polyfills.js';
            head.insertBefore(script, firstScriptTag);

            script.onload = function () {
                var testPromise = new Promise(function (resolve, reject) {
                    resolve('promise polyfill ready!');
                });
                testPromise.then(function (result) {
                    console.log(result);
                });

                var promise1 = new Promise(function (resolve, reject) {
                    resolve('Promise1');
                });

                var promise2 = new Promise(function (resolve, reject) {
                    resolve('Promise2');
                });

                Promise.all([promise1, promise2])
                    .then(function (values) {
                        console.log('testing Promise.all');
                        console.log(values);
                    })
                    .catch(function (err) {
                        console.log('err');
                        console.log(err);
                    });
            }
        }
        //sessionStorage:
        sessionStorage.setItem("in_sessionId", "J5025J4018020J20");
        
    </script>
</head>

<body>
    <div class="page current-learning">
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GVGY2R6BDW"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GVGY2R6BDW');
</script>
        <header class="page__header">
            <section class="banner">
                <div class="layout-container">
                    <div class="logo">
                        <a href="http://www.redhat.com/en"><span>REDHAT.COM</span></a>
                        <a href="http://www.redhat.com/en/customers"><span>Customers</span></a>
                        <a href="http://www.redhat.com/en/partners"><span>Partners</span></a>
                        <img class="logo--mobile" src="/st_toolkit/redhat/images/logo-mobile.png?6.17.47" alt="Red Hat Training" title="Red Hat Training"/>
                    </div>
                    <div class="notifications-nav" data-module="header__nav">
                      <div class="nav-control-mobile"><span class="top"></span><span class="middle"></span><span
                        class="bottom"></span></div>
                        <nav class="notifications" data-module="header__notifications">
                            <ul>
                                <li class="notif-badge-wrapper  notif-location"
                                    data-ajax="clmswidget.prMain?in_sessionid=J5025J4018020J20&in_rptName=LMT_WIDGET_MY_REGION&in_use_template=Y&in_cmins=480"
                                    data-fill-enable="Y" data-fill-browserCache="N" id="location">
                                </li>
                                <li class="notif-badge-wrapper  notif-emails"
                                    data-ajax="clmswidget.prMain?in_sessionid=J5025J4018020J20&in_rptName=LMT_WIDGET_MY_EMAILS&in_use_template=Y&in_cmins=5&rndval=LEARNERenin"
                                    data-fill-enable="Y" data-fill-browserCache="N" id="emails">
                                </li>
                                <li class="notif-badge-wrapper  notif-reminders"
                                    data-ajax="clmswidget.prMain?in_sessionid=J5025J4018020J20&in_rptName=LMT_WIDGET_MY_REMINDERS&in_use_template=Y&in_cmins=5&rndval=LEARNERenin"
                                    data-fill-enable="Y" data-fill-browserCache="Y" data-fill-keyBrowserCache="MyReminders" id="reminders">
                                </li>
                                <li class="notif-badge-wrapper  notif-shop-cart"
                                    data-ajax="clmswidget.prMain?in_sessionid=J5025J4018020J20&in_rptName=LMT_WIDGET_SHOPPING_CART&in_use_template=Y"
                                    data-fill-enable="Y" data-fill-enable="Y"
                                    data-fill-browserCache="N" id="shop-cart">
                                </li>
                                <li class="notif-badge-wrapper  notif-announce"
                                    data-ajax="clmswidget.prMain?in_sessionid=J5025J4018020J20&in_rptName=LMT_WIDGET_MY_ANNOUNCEMENTS&in_widget_rows=3&in_widget_start=1&in_use_template=Y&in_cmins=5&rndval=LEARNERenin"
                                    data-fill-enable="Y" data-fill-browserCache="Y"  data-fill-keyBrowserCache="MyAnnouncements" id="announce">
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="user-nav" data-module="header__nav">
                        <div class="nav-control-mobile"><span class="top"></span><span class="middle"></span><span
                                class="bottom"></span></div>
                        <nav class="user">
                            <ul class="custom-links ">
				<li >
					<a href="http://www.redhat.com/en"><span>REDHAT.COM</span></a>
				</li>
				<li >
					<a href="http://www.redhat.com/en/customers"><span>Customers</span></a>
				</li>
				<li >
					<a href="http://www.redhat.com/en/partners"><span>Partners</span></a>
				</li>
                            </ul>
                            <ul>
                                <li>
                                  <a class="help-top" href="#" onclick="openDialogBox('Help','clmsPages.prView?in_sessionid=J5025J4018020J20&in_page=HELP&in_selfContained=Y','', false,'','' ); return false;" title=""><span>Help</span></a>
                                </li>
                                
                                <li class="dd" data-module="user-nav" data-edit="CLMSCURRENTLEARNING.PRMAIN">
                                  <a href="#" class="user-options"><span>Harvinder Singh Diwan</span><div class="label-role">Learner</div></a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </section>
           <section class="sub-banner">
              <div class="layout-container "><img class="logo--desktop" src="/st_toolkit/redhat/images/logo.png?6.17.47" alt="Red Hat Training" title="Red Hat Training"/></div>
           </section>
            <section class="header">
                <div class="layout-container">
                    
        <div class="primary-nav" data-module="header__nav">
          <div class="nav-control-mobile"><span class="top"></span><span class="middle"></span><span class="bottom"></span></div>
          <nav class="main">

<ul>

<li class="first nav-home"><a href="lmtLogin.prMenu?in_sessionid=J5025J4018020J20"><span>Home</span></a>

</li>

<li class="dd nav-catalog"><a href="javascript:void(0);" title="Catalogue"><span>Catalogue</span></a>

<ul>

<li><a class="subnav-browse" href="clmsbrowsev2.prMain?in_sessionid=J5025J4018020J20" title="Browse">Browse</a>

</li>

<li><a class="subnav-request_learning" href="clmaRequestExtBooking.prMain?in_sessionid=J5025J4018020J20">Request Learning</a>

</li>

<li><a class="subnav-request_learning" href="clmaRequestExtBooking.prMain?in_sessionid=J5025J4018020J20&in_section=V3">Request Lab</a>

</li>

<li><a class="subnav-calendar" href="clmsCourseCalendar.prMain?in_sessionid=J5025J4018020J20&in_portid=1" title="Search Calendar">Search Calendar</a>

</li>

</ul>

</li>

<li class="dd prim-nav-active active nav-learning"><a href="javascript:void(0);"><span>My Learning</span></a>

<ul>

<li><a class="subnav-my_current_learning" href="clmsCurrentLearning.prMain?in_sessionid=J5025J4018020J20">My Current Learning</a>

</li>

<li><a class="subnav-my_learning_history" href="clmslearninghistory.prmain?in_sessionid=J5025J4018020J20">My Learning History</a>

</li>

<li><a class="subnav-my_subscriptions" href="clmssubscriptions.prMain?in_sessionid=J5025J4018020J20">My Subscriptions</a>

</li>

</ul>

</li>

<li class="dd nav-report"><a href="javascript:void(0);"><span>Reporting</span></a>

<ul>

<li><a class="subnav-my_reports" href="clmareports.prsummary?in_sessionid=J5025J4018020J20&in_autoquery=Y">My Reports</a>

</li>

<li><a href="clmamyreporthistory.prmain?in_sessionid=J5025J4018020J20">My Report History</a>

</li>

</ul>

</li>

<li class="dd"><a href="javascript:void(0);"><span>My Orders</span></a>

<ul>

<li><a class="subnav-my_orders" href="clmaOrders.prMyOrders?in_sessionid=J5025J4018020J20">My Orders</a>

</li>

<li><a class="subnav-licence_keys" href="clmsactivatekey.prmain?in_sessionid=J5025J4018020J20">Enrolment Keys</a> 

</li>

</ul>

</li>

</ul>

          </nav>
        </div>

                        <div class="search-bar">
                            <div class="search" data-module="search__nav">
                                <form data-mode="expanded" id="headerSearchForm" action="clmsbrowseV2.prMain" method="post" name="crsForm2" class="form2 active search--active">
         <input type="hidden" name="in_sessionid" value="J5025J4018020J20"/>
         <input name="in_auto_search" value="Y" type="hidden"/>
        <div class="form-field">
          <label for="headerSearchForm_searchCatalogue" class="sr-only">Search Catalogue</label>
          <input name="in_keyword" type="text" maxlength="200" class="rhs-search-input" placeholder="Search Catalogue" id="headerSearchForm_searchCatalogue"/>
          <button type="submit" id="activateSearchForm" class="icon icon-search active search--active"></button>
        </div>
      </form>
                            </div>
                        </div>
                </div>
            </section>
        </header>
        <main><section class="current-learning__intro">
  <h1>My Current Learning</h1>
</section>
<section class="search-results" data-module="search-results-post">
  <div class="search-results__search">
    <form action="clmsCurrentLearning.prFormAction" method="post" name="crsForm" data-ajax="false" id="crsForm" class="search-form">
         <input type="hidden" name="in_sessionid" value="J5025J4018020J20">
         <input type="hidden" name="in_edition" value="">
         <input type="hidden" name="in_from_module" value="LMTLOGIN.PRMENU">
         <input type="hidden" name="in_filter" value="">         
         <input type="hidden" name="in_filter_2" value="">         
         <input type="hidden" name="in_tab_group" value="">       
         <input type="hidden" name="in_orderBy" value="NA">           
         <input type="hidden" name="in_learner_id" value="">
         <input type="hidden" name="in_selfContained" value="">
         <input type="hidden" name="in_admin_header" value="">
         <input type="hidden" name="in_displayForm" value="Y"/>
      <div class="form__row">
        <label for="in_courseName" class="form_group">
          <span>Course Name</span>
          <input type="text" name="in_courseName" value="">
        </label>
        <label for="in_courseType" class="form_group">
          <span>Course Type</span>
          <select class="ctrl drp" id="id_courseType" name="in_courseType"><option value="">All</option><option value="C">Classroom</option><option value="EX">Exam</option><option value="ES">Expert Seminars</option><option selected value="K">Individual Exam</option><option value="LR">Lab Rental</option><option value="L">Online Lab</option><option value="R">Online Learning</option><option value="OS">Onsite</option><option value="OSVT">Onsite Virtual Training</option><option value="RC">Remote Classroom</option><option value="RONS">Request Onsite</option><option value="SN">Subscription</option><option value="VC">Video Classroom</option><option value="VT">Virtual Training</option></select>
        </label>
      <div class="form__row">
      <div class="btn__group">
          
        </div>


      </div>
        <input type="submit" value="Search" class="btn btn--submit" />
        <input type="button" value="Clear" class="btn btn--clear" />
      </div>
    </form>
  </div>
<div class="search-results__filter" data-module="search-results-post__filter" data-total-rows="4" data-rows-fetch="50" data-sort-filter-escape="%26in_courseType%3DK" data-initial-orderby="NA" data-sort-report="LMT_WIDGET_CURRENT_LEARNING_SORT" data-sort-template="LMT_WIDGET_CURRENT_LEARNING_SORT_V3" data-moreresults-report="clmsCurrentLearning.prMain?in_sessionId=J5025J4018020J20&in_from_module=LMTLOGIN.PRMENU&in_courseType=K&in_displayForm=N">
    <div class="filter__results-controls">
      <div class="filter__results--total">
        <span class="label--total"><span>4</span> Results</span>
      </div>

      <div class="filter__sort-wrapper"></div>

      <!-- <a href="#" class="filter__view-toggle icon-list-view" data-module="filter__view-toggle" aria-label="Switch to List View"></a> -->
      <a href="#" data-current-view="card" data-save-state="clmsnode.prModuleState?in_sessionid=J5025J4018020J20&in_module=CLMSCURRENTLEARNING.PRMAIN&in_type=view" class="filter__view-toggle icon-list-view" data-module="filter__view-toggle" aria-label="Switch View"></a>
    </div>
  </div>

<div class="search-results__result result-list card-view" data-module="search-results-post__result">
<div class="card course--">
  <div class="media"><span title="Individual Exam" class="pictogram modality-K "></span></div>
  <div class="card__body result-detail">
    <div class="meta-data labels">
      <span class="label--type">Individual Exam</span>
      
    </div>
    <h4 class="title"><a href="clmsCourseDetails.prMain?in_sessionId=J5025J4018020J20&in_offeringId=44548927&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group="><span>Red Hat Certified Specialist in Ceph Cloud Storage exam (EX260V50K)</span></a></h4> 
    <div class="meta-data body">
		<span class="list-pill">Not Attempted</span>
		
		
		
		 
    </div>
    <div class="description"></div>
    <div class="ratings"></div>
     
    <div class="cta"><a href="clmscoursedetails.prmain?in_sessionid=J5025J4018020J20&in_offeringid=44548927&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group=" class="action small view "><span>Details</span></a> </div>
  </div>
</div>
<div class="card course--">
  <div class="media"><span title="Individual Exam" class="pictogram modality-K "></span></div>
  <div class="card__body result-detail">
    <div class="meta-data labels">
      <span class="label--type">Individual Exam</span>
      
    </div>
    <h4 class="title"><a href="clmsCourseDetails.prMain?in_sessionId=J5025J4018020J20&in_offeringId=41424113&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group="><span>Red Hat Certified Specialist in Cloud Infrastructure Exam (EX210V13K) -IN</span></a></h4> 
    <div class="meta-data body">
		<span class="list-pill">Completed - Repeat</span>
		
		
		
		 
    </div>
    <div class="description"></div>
    <div class="ratings"></div>
     
    <div class="cta"><a href="clmscoursedetails.prmain?in_sessionid=J5025J4018020J20&in_offeringid=41424113&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group=" class="action small view "><span>Details</span></a> </div>
  </div>
</div>
<div class="card course--">
  <div class="media"><span title="Individual Exam" class="pictogram modality-K "></span></div>
  <div class="card__body result-detail">
    <div class="meta-data labels">
      <span class="label--type">Individual Exam</span>
      
    </div>
    <h4 class="title"><a href="clmsCourseDetails.prMain?in_sessionId=J5025J4018020J20&in_offeringId=44640139&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group="><span>Red Hat Certified Specialist in Linux Diagnostics and Troubleshooting exam (EX342V84K)</span></a></h4> 
    <div class="meta-data body">
		<span class="list-pill">Not Attempted</span>
		
		
		
		 
    </div>
    <div class="description"></div>
    <div class="ratings"></div>
     
    <div class="cta"><a href="clmscoursedetails.prmain?in_sessionid=J5025J4018020J20&in_offeringid=44640139&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group=" class="action small view "><span>Details</span></a> </div>
  </div>
</div>
<div class="card course--">
  <div class="media"><span title="Individual Exam" class="pictogram modality-K "></span></div>
  <div class="card__body result-detail">
    <div class="meta-data labels">
      <span class="label--type">Individual Exam</span>
      
    </div>
    <h4 class="title"><a href="clmsCourseDetails.prMain?in_sessionId=J5025J4018020J20&in_offeringId=41304446&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group="><span>Red Hat Certified Specialist in Security: Linux exam (EX415V75K) - IN</span></a></h4> 
    <div class="meta-data body">
		<span class="list-pill">Completed - Repeat</span>
		
		
		
		 
    </div>
    <div class="description"></div>
    <div class="ratings"></div>
     
    <div class="cta"><a href="clmscoursedetails.prmain?in_sessionid=J5025J4018020J20&in_offeringid=41304446&in_selfContained=N&in_from_module=LMTLOGIN.PRMENU&in_filter=%26in_courseType%3DK%26in_orderBy%3DNA%26in_from_module%3DLMTLOGIN.PRMENU%26in_courseGroup%3DAll&in_filter_2=&in_tab_group=" class="action small view "><span>Details</span></a> </div>
  </div>
</div>
</div><!-- .card-view.result-list -->
<div class="search-results__moreresults" data-module="search-results-post__moreresults">
    <a href="#" class="load-more__button" aria-roledescription="SHOW MORE BUTTON" data-start="">Show More</a>
</div>
</section>

        <div class="sys-intg-error" style="display: none;"><span>The LMS is currently updating its back-end systems. Users will not be able to process orders during this time. All other functionality is active during this maintenance period.</span></div>
        <script>
          if ("N" == "Y") {
            document.querySelector(".sys-intg-error").style.display = "block";
          }
        </script>
      </main>

      <section class="footer">
        <footer>
            <div>
            © 2022 Red Hat Inc.
            </div>
            <ul>
                <li><a href="clmaRequestExtBooking.prMain?in_sessionId=J5025J4018020J20&in_section=V2">Not Finding the Right Class?</a></li>
                <li><a href="http://www.redhat.com/en/about/privacy-policy">Privacy Statement</a></li>
                <li><a href="http://www.redhat.com/en/about/terms-use" target="_blank">Terms of Use</a></li>
                <li><a href="https://www.redhat.com/en/about/red-hat-training-policies" target="_blank">Training Terms and Conditions</a></li>
                <li><a href="http://www.redhat.com/en/about/all-policies-guidelines" target="_blank">All policies and guidelines</a></li>
            </ul>
            <aside>
                <a href="http://www.redhat.com/summit/" target="_blank"><img src="/st_toolkit/redhat/images/icon-summit.png" alt="Summit logo"/></a>
            </aside>
        </footer>
      </section>
      <!-- begin adobe analytics DTM tracking -->
<script type="text/javascript">
    if (("undefined" !== typeof _satellite) && ("function" === typeof _satellite.pageBottom)) {
        _satellite.pageBottom();
    }
</script> 
<!-- end adobe analytics DTM tracking -->
<script>
var gPageName = "my current learning";
</script>
    </div><!-- page type wrapper -->

    <script defer type="text/javascript" src="/st_toolkit/common/script/pages/current-learning.js?6.17.47"></script>
<!-- <script type="text/javascript" src="/st_toolkit/common/script/common.js?6.17.47"></script>  -->
  </body>
</html>

</body>
</html>
